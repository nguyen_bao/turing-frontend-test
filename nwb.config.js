module.exports = {
  type: 'react-app',
  webpack: {
    extra: {
      node: {
        fs: 'empty'
      }
    }
  }
}
