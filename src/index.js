
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'

import React from 'react'
import {render} from 'react-dom'
import store from './store';
import Root from './root';

render(<Root store={store}/>, document.querySelector('#app'))
