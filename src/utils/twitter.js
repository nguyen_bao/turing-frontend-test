const Twitter = require('twitter')
import config from './config.json'
const client = new Twitter({
    consumer_key: config.consumer_api,
    consumer_secret: config.consumer_secret,
    access_token_key: config.access_token,
    access_token_secret: config.access_token_secret
})

export async function user_timeline(screen_name, count = 30) {
    return new Promise((resolve, reject) => {
        let params = {
            screen_name: screen_name,
            count: count
        }
        client.get('statuses/user_timeline', params, (error, tweets, response) => {
            if (error) {
                return reject(error)
            }
            return resolve(tweets)
        })
    })
}
