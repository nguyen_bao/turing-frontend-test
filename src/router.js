import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import TwitterContainer from './components/container/twitter_container'

const Routers = () => (
    <Router>
        <Route exact path="/" component={TwitterContainer}/>
    </Router>
)

export default Routers