// @flow
import type {Setting} from '../types/setting'
import {UPDATE_SETTING} from '../types/setting'

const getSetting = ():Setting => {
    let currentSetting = localStorage.getItem(UPDATE_SETTING)
    currentSetting = currentSetting ? JSON.parse(currentSetting) : {}
    return {
        color: 'primary',
        showMakeSchool: true,
        showNewsycombinator: true,
        showYcombinator: true,
        ...currentSetting
    }
} 

const initialState: Setting = getSetting()

const setting = (state:any = initialState, action:any) => {
    switch(action.type) {
        case UPDATE_SETTING:
            return {
                ...state, 
                ...action.data
            }
    }
    return state
}

export default setting