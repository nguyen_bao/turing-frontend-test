// @flow
import type {Tweet} from '../types/tweet'
import {START_FETCH_TWEETS, SUCCESS_FETCH_TWEETS, FAILED_FETCH_TWEETS} from '../types/tweet'

type TwitterState = {
    makeschool: Array<Tweet>,
    newsycombinator: Array<Tweet>,
    ycombinator: Array<Tweet>,
    loading: boolean
}

const initialState: TwitterState = {
    makeschool: [],
    newsycombinator: [],
    ycombinator: [],
    loading: false
}

const twitter = (state:TwitterState = initialState, action:any) => {
    
    switch(action.type) {
        case START_FETCH_TWEETS:
            return { 
                ...state,
                loading: true
            }
        case SUCCESS_FETCH_TWEETS:
            return {
                ...state,
                ...action.data,
                loading: false
            }
        default:
            return {
                ...state
            }
    }
}

export default twitter