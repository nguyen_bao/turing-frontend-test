var twitter = require('./utils/twitter')
import {
    convertResponseToTweet
} from './types/tweet'
var express = require('express')
var app = express()

app.use(require('nwb/express')(express))

app.get('/tweets', async function(req, res, next) {
    let makeschools = await twitter.user_timeline("makeschool")
    let newsycombinator = await twitter.user_timeline('newsycombinator')
    let ycombinator = await twitter.user_timeline('ycombinator')
    
    res.json({
        makeschool: convertResponseToTweet(makeschools),
        newsycombinator: convertResponseToTweet(newsycombinator),
        ycombinator: convertResponseToTweet(ycombinator)
    })
})

app.listen(3000, function (err) {
    if (err) {
        console.error('error starting server:')
        console.error(err.stack)
        process.exit(1)
    }
    console.log('server listening at http://localhost:3000')
})