// @flow

export type Setting = {
    color: string,
    showMakeSchool: boolean,
    showNewsycombinator: boolean,
    showYcombinator: boolean
}

export const UPDATE_SETTING = 'UPDATE_SETTING'