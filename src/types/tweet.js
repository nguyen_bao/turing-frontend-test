/* @flow */
export type Tweet = {
    screen_name: string,
    created_at: string,
    link: string,
    retweet: string,
    content: string
}

export const START_FETCH_TWEETS = 'START_FETCH_TWEETS'
export const SUCCESS_FETCH_TWEETS = 'SUCCESS_FETCH_TWEETS'
export const FAILED_FETCH_TWEETS = 'FAILED_FETCH_TWEETS'

const formatDate = (date) => {
    return date.replace(/\+0000/, "").replace(/\d{2}:\d{2}:\d{2}/, "")
}

const makeLink = (tweet) => {
    return `https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`
}

export const convertResponseToTweet = (data: Array<any>):Array<Tweet> => {
    return  data.map((item:any) => {
        return {
            screen_name: item.user.screen_name,
            link: makeLink(item),
            content: item.text,
            created_at: formatDate(item.created_at),
            retweet: item.retweeted_status == undefined ? '' : item.retweeted_status.user.screen_name
        }
    });
}