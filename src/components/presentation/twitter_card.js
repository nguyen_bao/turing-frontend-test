// @flow
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Card, CardTitle, CardSubtitle, CardText, Row, Col, Button} from 'reactstrap'
import type {Tweet} from '../../types/tweet'

const Styles = {
    margin: "1px"
}

type Props = {
    tweet: Tweet,
    color: String
}

class TwitterCard extends Component<Props> {
    render() {
        let {tweet, color} = this.props

        return (
            <Row style={Styles}>
                <Col md="12">
                <Card body inverse color={color}>
                    <CardTitle>@{tweet.screen_name}</CardTitle>
                    <CardSubtitle>Tweeted at {tweet.created_at}</CardSubtitle>
                    <CardText>
                        {tweet.content}
                    </CardText>
                    <CardText>
                        <small>Retweeted: {tweet.retweet}</small>
                    </CardText>
                    <Button color={color} onClick={() => {window.open(tweet.link)}}>View on twitter</Button>
                </Card>
                </Col>
            </Row>
        )
    }
}

export default TwitterCard