// @flow
import React, {Component} from 'react'
import TwitterCard from './twitter_card'
import type {Tweet} from '../../types/tweet'

type Props = {
    tweets: Array<Tweet>,
    color: String
}

class TwitterColumn extends Component<Props> {
    render() {
        let {tweets, color} = this.props
        let cards = tweets.map((item, idx) => {
            return (
                <TwitterCard tweet={item} color={color} key={idx} />
            )
        })
        return (
            cards
        )
    }
}

export default TwitterColumn