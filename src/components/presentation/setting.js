// @flow
import React, {Component} from 'react'
import {
    Select,
    MenuItem,
    FormControl,
    InputLabel,
    FormLabel,
    FormControlLabel,
    Checkbox
} from '@material-ui/core'
import {withStyles} from '@material-ui/core/styles'


type Props = {
    color: String,
    showMakeSchool: boolean,
    showNewsycombinator: boolean,
    showYcombinator: boolean,
    updateSetting: Function,
    classes: Object
}

type State = {
    isOpen: boolean
}

const style = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    }
})

class Setting extends Component<Props, State> {

    onItemClick: Function

    constructor(props:Props) {
        super(props)
        console.log(props)
        this.onItemClick = this.onItemClick.bind(this)
        this.state = {
            isOpen: false
        }
    }

    onItemClick(event:any) {
        console.log(event.target.value)
        this.props.updateSetting({
            color: event.target.value
        })
    }

    onShowTweet = name => event => {
        this.props.updateSetting({
            [name]: event.target.checked 
        })
    }

    render() {
        let {classes, color, showMakeSchool, showNewsycombinator, showYcombinator} = this.props
        return (
        <form className={this.props.classes.root} autoComplete="off">
            <FormControl className={this.props.classes.formControl}>
                <InputLabel>Card Type</InputLabel>
                <Select
                    value={this.props.color}
                    onChange={this.onItemClick}
                >
                    <MenuItem value={"primary"}>Primary</MenuItem>
                    <MenuItem value={"success"}>Success</MenuItem> 
                    <MenuItem value={"info"}>Info</MenuItem>
                    <MenuItem value={"warning"}>Warning</MenuItem>
                    <MenuItem value={"danger"}>Danger</MenuItem>
                </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
                <FormLabel component="legend">Show Tweets</FormLabel>
                <FormControlLabel
                    control={
                        <Checkbox checked={showMakeSchool} onChange={this.onShowTweet('showMakeSchool')}/>
                    }
                    label="@MakeSchool"
                />
                <FormControlLabel
                    control={
                        <Checkbox checked={showYcombinator} onChange={this.onShowTweet('showYcombinator')}/>
                    }
                    label="@ycombinator"
                />
                <FormControlLabel
                    control={
                        <Checkbox checked={showNewsycombinator} onChange={this.onShowTweet('showNewsycombinator')}/>
                    }
                    label="@newsycombinator"
                />
            </FormControl>
        </form>
        )
    }
}

export default withStyles(style)(Setting)