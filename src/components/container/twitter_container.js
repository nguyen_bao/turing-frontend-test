// @flow 
import React, {Component} from 'react'
import {connect} from 'react-redux'
import type {Tweet} from '../../types/tweet'
import {Row, Col, Container} from 'reactstrap'
import TwitterColumn from '../presentation/twitter_column'
import Setting from '../presentation/setting'
import {fetchTweets} from '../../actions/twitter'
import {updateSetting} from '../../actions/setting'
import {withStyles, createMuiTheme} from '@material-ui/core/styles'
import classNames from 'classnames'
import { Toolbar, 
    Drawer, 
    AppBar, 
    CssBaseline, 
    Typography,
    CircularProgress,
    MuiThemeProvider,
    IconButton,
    Divider
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'



type Props = {
    makeschool: Array<Tweet>,
    newsycombinator: Array<Tweet>,
    ycombinator: Array<Tweet>,
    showMakeSchool: boolean,
    showNewsycombinator: boolean,
    showYcombinator: boolean,
    fetchTweets: Function,
    updateSetting: Function,
    classes: Object,
    color: String,
    loading: boolean
}

type States = {
    open: boolean
}

const drawerWidth = 240

const styles = theme => ({
    root: {
        display: "flex",
    },
    appBar: {
        color: "#ffffff",
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20
    },
    hide: {
        display: 'none'
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: '0 8px',
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end'
    },
    progress: {
        display: 'flex',
        alignItems: 'center',
        ...theme.mixins.toolbar,
        justifyContent: 'center'
    },
    content: {
        flexGrow: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
    toolbar: theme.mixins.toolbar
})

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#2196f3"
        }
    }
})

class TwitterContainer extends Component<Props, States> {

    constructor(props) {
        super(props)
        this.state = {
            open: false
        }
        this.handleDrawerOpen = this.handleDrawerOpen.bind(this)
        this.handleDrawerClose = this.handleDrawerClose.bind(this)
    }

    componentDidMount() {
        this.props.fetchTweets()
    }

    handleDrawerOpen = () => {
        this.setState(previousState => ({
            ...previousState,
            open: true
        }))
    }

    handleDrawerClose = () => {
        this.setState(previousState => ({
            ...previousState,
            open: false
        }))
    }

    render() {
        let {makeschool, newsycombinator, ycombinator, color, loading, showMakeSchool, showYcombinator, showNewsycombinator} = this.props
        let {open} = this.state
        return (
            <MuiThemeProvider theme={theme}>
            <Row className={this.props.classes.root}>
                <CssBaseline />
                <AppBar position="fixed" className={classNames(this.props.classes.appBar, {
                    [this.props.classes.appBarShift] : open
                })}>
                    <Toolbar disableGutters={!open}>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerOpen}
                            className={classNames(this.props.classes.menuButton, open && this.props.classes.hide)}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            Twitter Api
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Drawer 
                    className={this.props.classes.drawer} 
                    variant="persistent" 
                    anchor="left"
                    open={open}
                    classes={{paper: this.props.classes.drawerPaper}}
                >
                    <div className={this.props.classes.drawerHeader}>
                        <IconButton onClick={this.handleDrawerClose}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider/>
                    <Setting updateSetting={this.props.updateSetting} 
                        showMakeSchool={showMakeSchool}
                        showYcombinator={showYcombinator}
                        showNewsycombinator={showNewsycombinator}
                        color={color}/>
                </Drawer>
                <main className={classNames(this.props.classes.content,{
                    [this.props.classes.contentShift]: open,
                })}>
                    <div className={this.props.classes.drawerHeader}/>
                    { loading ? 
                        <div className={this.props.classes.progress}>
                        <CircularProgress color="primary" /> 
                        </div>
                        : null
                    }
                    <Container>
                        <Row>
                            {
                                showMakeSchool ? 
                                <Col md="4" xl="4" xs="12" sm="6">
                                    <TwitterColumn color={color} tweets={makeschool}/>
                                </Col>
                                : null
                            }
                            {
                                showNewsycombinator ? 
                                <Col md="4" xl="4" xs="12" sm="6">
                                    <TwitterColumn color={color} tweets={newsycombinator}/>
                                </Col>
                                : null
                            }
                            {
                                showYcombinator ? 
                                <Col md="4" xl="4" xs="12" sm="6">
                                    <TwitterColumn color={color} tweets={ycombinator}/>
                                </Col>
                                : null
                            }                                                        
                        </Row>
                    </Container>
                </main>
            </Row>
            </MuiThemeProvider>
        )
    }
}

const mapStateToProps = state => {
    return {
        makeschool: state.twitter.makeschool,
        newsycombinator: state.twitter.newsycombinator,
        ycombinator: state.twitter.ycombinator,
        loading: state.twitter.loading,
        color: state.setting.color,
        showMakeSchool: state.setting.showMakeSchool,
        showYcombinator: state.setting.showYcombinator,
        showNewsycombinator: state.setting.showNewsycombinator
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchTweets: () => dispatch(fetchTweets()),
        updateSetting: (setting) => dispatch(updateSetting(setting))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(TwitterContainer))

