import {combineReducers} from 'redux'
import twitter from './reducers/twitters'
import setting from './reducers/setting'

export default combineReducers({
    twitter,
    setting
});