// @flow
import {UPDATE_SETTING} from '../types/setting'

export const updateSetting = (setting: any) => {
    let currentSetting = localStorage.getItem(UPDATE_SETTING)
    currentSetting = currentSetting ? JSON.parse(currentSetting) : {}
    localStorage.setItem(UPDATE_SETTING, JSON.stringify({
        ...currentSetting,
        ...setting
    }))
    return {
        type: UPDATE_SETTING,
        data: setting
    }
}