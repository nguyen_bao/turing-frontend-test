// @flow
import axios from 'axios'

import {
    START_FETCH_TWEETS, 
    SUCCESS_FETCH_TWEETS, 
    FAILED_FETCH_TWEETS, 
    convertResponseToTweet
} from '../types/tweet'

export const fetchTweets = () => {
    return async (dispatch: any) => {
        console.log("START GET TWEETS")
        dispatch({type: START_FETCH_TWEETS})
        let response = await axios.get('/tweets')
        console.log(response.data)
        dispatch({type: SUCCESS_FETCH_TWEETS, data: response.data})

    }
}