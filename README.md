# turing-frontend-test

Using Twitter API to fetch the tweets of @MakeSchool, @newsycombinator, @ycombinator and display these tweets using React. 
This project is using the ```nwb``` module and React, React-Redux to display the information. 

# Getting Started 
After unarchiving the zip file, please install the dependencies using the following command 

```bash
$ yarn install
```

If you don't have ```yarn```, you can use ```npm`` instead. 

```bash 
$ npm install
```

After all dependencies are installed in the folder, run the following command to start the server 
```bash
$ yarn server 
```

or with ```npm```
```bash
$ npm run server
```

Navigate the [http://localhost:3000]() to view the application. 

# Override your Twitter credential 

I already removed the twitter credential. Please kindly add your Twitter credential in ```src/utils/config.json```. 
```json
{
    "consumer_api": "YOU_CONSUMER_API",
    "consumer_secret": "YOUR_CONSUMER_SECRET",
    "access_token": "YOUR_ACCESS_TOKEN",
    "access_token_secret": "YOUR_ACCESS_TOKEN_SECRET"
}
```


